package main

import (
	"bufio"
	"flag"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/Spouk/gotool/config"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"io"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

const logprefix = "[work-apcups-checker] "
const logflags = log.LstdFlags | log.Lshortfile

type ConfigStruct struct {
	Debug    bool          `yaml:"debug"`
	Timeout  time.Duration `yaml:"timeout"`
	UserBase string        `yaml:"userbase"`
	PassBase string        `yaml:"passbase"`
	Servers  []*server     `yaml:"servers"`
}
type server struct {
	IP       string            `yaml:"ip"`
	Port     string            `yaml:"sshport"`
	User     string            `yaml:"sshuser"`
	Password string            `yaml:"sshpassword"`
	Commands map[string]string `yaml:"commands"`
}

type checkResult struct {
	IP           string
	UpsilonDir   string
	ApcupsDir    string
	UpsilonDemon string
	ApcupsDemon  string
	USB          string
}

type WorkApcups struct {
	log          *log.Logger
	cfg          *ConfigStruct
	Result       []*checkResult
	Lock         sync.RWMutex
	TW           *tablewriter.Table
	ErrorServers []*ErrorServer
}

type ErrorServer struct {
	S     *server
	Error string
}

var (
	user        string
	pass        string
	configParam string
	cli         bool //флаг показывающий,что использовать данные из параметров консоли а не из конфига для авторизации на удаленных машинах
	generator   bool
	genfile     string
	resultgen   bool
	debuger     bool
	force       bool
)

func main() {
	flag.BoolVar(&force, "force", false, "использовать при наличии данных для авторизации в полях конфига серверов принудительно в любом случае")
	flag.BoolVar(&debuger, "debug", false, "дебаг вывода")
	flag.BoolVar(&generator, "generator", false, "флаг генерации конфига из списка серверов")
	flag.BoolVar(&resultgen, "rf", false, "флаг создания читаемого отчета")
	flag.StringVar(&genfile, "genfile", "", "файл со списком серверов в формате 127.0.0.1:22")
	flag.StringVar(&user, "user", "", "базовый пользователь под которым производить подключения к удаленным машинам")
	flag.StringVar(&pass, "pass", "", "пароль пользователя под которым производится операции на удаленной машине")
	flag.StringVar(&configParam, "config", "", "файл конфигурации с полным путем")
	flag.BoolVar(&cli, "cli", false, "флаг, определяющий откуда брать данные пользователя под которым проходит добавление,консоль или конфиг")
	flag.Parse()

	//генератор
	if generator && genfile != "" {
		m := NewWorkApcups(configParam, true)
		m.GeneratorConfig(genfile)
		os.Exit(1)
	}

	//добавление с данными из консоли
	if cli && configParam != "" && user != "" && pass != "" || cli && configParam != "" && user != "" && pass != "" && resultgen {
		m := NewWorkApcups(configParam, false)
		m.log.Println("=> [user mode] ")

		sw := &sync.WaitGroup{}
		for _, x := range m.cfg.Servers {
			//проверка force
			if force == false && (x.Password == "" || x.User == "") {
				x.User = user
				x.Password = pass
			}
			sw.Add(1)
			go m.Checkhost(x, sw)
		}
		sw.Wait()
		m.log.Println("завершение работы")
		if resultgen {
			m.GeneratingTableResult(os.Stdout)
			m.GeneratingTableResulFAIL(os.Stdout)
			m.GeneratingTableResultERRORSERVERS(os.Stdout)
		} else {
			m.ShowResult()
		}

		//добавление с данными из конфига
	} else if cli == false && configParam != "" || cli == false && configParam != "" && resultgen {
		m := NewWorkApcups(configParam, false)
		m.log.Println("==> [config mode]")

		sw := &sync.WaitGroup{}
		for _, x := range m.cfg.Servers {
			sw.Add(1)
			go m.Checkhost(x, sw)
		}
		sw.Wait()
		m.log.Println("завершение работы")
		if resultgen {
			m.GeneratingTableResult(os.Stdout)
			m.GeneratingTableResulFAIL(os.Stdout)
			m.GeneratingTableResultERRORSERVERS(os.Stdout)
		} else {
			m.ShowResult()
		}

	} else {
		flag.PrintDefaults()
		os.Exit(1)
	}
}

//создаю инстанс
func NewWorkApcups(fconfig string, emptyGen bool) *WorkApcups {
	w := &WorkApcups{
		log: log.New(os.Stdout, logprefix, logflags),
	}
	if emptyGen {
		return w
	}
	tmpcfg := &ConfigStruct{}
	cc := config.NewConf("/tmp/", os.Stdout)
	err := cc.ReadConfig(fconfig, tmpcfg)
	if err != nil {
		w.log.Panic(err)
	}
	w.cfg = tmpcfg
	return w
}

//выполнение  external-команды в рамках существующей сессии
func (w *WorkApcups) exec(c *ssh.Client, key, command string) (string, string) {
	cc, err := c.NewSession()
	if err != nil {
		w.log.Printf("Ошибка создания сессии: %v\n", err)
	}

	res, err := cc.CombinedOutput(command)
	if err != nil {
		log.Printf("[%s] Ошибка выполнения команды: %v : %s\n", key, err, string(res))
	}
	return key, string(res)
}

//установка ssh соединения с удаленной машиной
func (w *WorkApcups) connectssh(s *server) (*ssh.Client, error) {
	w.log.Printf("[%v] %s %s\n", s.IP, s.Port, s.User)
	cc := &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            s.User,
		Auth: []ssh.AuthMethod{
			ssh.Password(s.Password),
		},
		Timeout: time.Second * w.cfg.Timeout,
	}
	connection, err := ssh.Dial("tcp", net.JoinHostPort(s.IP, s.Port), cc)
	if err != nil {
		return nil, err
	}
	return connection, nil
}

//выполнение на удаленной машине в рамках сессии списка команд::горутина-рабочий
func (w *WorkApcups) Checkhost(s *server, sw *sync.WaitGroup) {
	defer sw.Done()
	res := &checkResult{}
	res.IP = s.IP
	con, err := w.connectssh(s)
	if debuger {
		w.log.Printf("[debuger][Checkhost - `%v`] %v  `%v`\n", s.IP, err, s)
	}
	if err != nil {
		w.log.Printf("[Checkhost - `%v`] %v\n", s.IP, err)
		w.Lock.Lock()
		e := &ErrorServer{}
		e.S = s
		e.Error = err.Error()
		w.ErrorServers = append(w.ErrorServers, e)
		w.Lock.Unlock()
		return
	}
	for k, v := range s.Commands {
		key, result := w.exec(con, k, v)
		switch key {
		case "upsdir":
			res.UpsilonDir = result
		case "apsdir":
			res.ApcupsDir = result
		case "upsdemon":
			res.UpsilonDemon = result
		case "apsdemon":
			res.ApcupsDemon = result
		case "usb":
			res.USB = result
		}
	}
	w.Lock.Lock()
	w.Result = append(w.Result, res)
	w.Lock.Unlock()
}

//----------------------------------------------------------------------
// table output
// Сервер (u/w)         |    APCUPS (DIR)   |   APCUPS (DEMON) |  UPSILON (DIR)  | UPSILON (DEMON)
// --------------------------------------------------------------------------------------
// 172.30.32.10(u)         есть                 запущен           нет             не запущен
//-------------------------------------------------------------------------------------
//формирование выходной таблицы
func (w *WorkApcups) GeneratingTableResult(out io.Writer) {
	table := tablewriter.NewWriter(out)
	table.SetHeader([]string{"Сервер (u/w)", "APCUPS (dir)", "APCUPS (demon)", "UPSILON (dir)", "UPSILON (demon)", "UPSILON (USB)", "CONTROL"})
	data := [][]string{}

	for _, x := range w.Result {
		tmp := []string{x.IP}

		//a.d
		if strings.HasPrefix(x.ApcupsDir, "0") {
			tmp = append(tmp, "-")
		} else if strings.HasPrefix(x.ApcupsDir, "1") {
			tmp = append(tmp, "+")
		} else {
			tmp = append(tmp, "err.check")
			if debuger {
				w.log.Printf("[debug] [%v] %v\n", x.IP, x.ApcupsDir)
			}
		}
		//a.s
		if strings.HasPrefix(x.ApcupsDemon, "0") {
			tmp = append(tmp, "-")
		} else if strings.HasPrefix(x.ApcupsDemon, "1") {
			tmp = append(tmp, "+")
		} else {
			tmp = append(tmp, "err.check")
			if debuger {
				w.log.Printf("[debug] [%v] %v\n", x.IP, x.ApcupsDemon)
			}
		}

		//u.d
		if strings.HasPrefix(x.UpsilonDir, "0") {
			tmp = append(tmp, "-")
		} else if strings.HasPrefix(x.UpsilonDir, "1") {
			tmp = append(tmp, "+")
		} else {
			tmp = append(tmp, "err.check")
			if debuger {
				w.log.Printf("[debug] [%v] %v\n", x.IP, x.UpsilonDir)
			}
		}

		//u.s
		if strings.HasPrefix(x.UpsilonDemon, "0") {
			tmp = append(tmp, "-")
		} else if strings.HasPrefix(x.UpsilonDemon, "1") {
			tmp = append(tmp, "+")
		} else {
			tmp = append(tmp, "err.check")
			if debuger {
				w.log.Printf("[debug] [%v] %v\n", x.IP, x.UpsilonDemon)
			}
		}

		//usb
		if strings.HasPrefix(x.USB, "0") {
			tmp = append(tmp, "-")
		} else if strings.HasPrefix(x.USB, "1") {
			tmp = append(tmp, "+")
		} else {
			tmp = append(tmp, "err.check[no usbutils pack]")
			if debuger {
				w.log.Printf("[debug] [%v] %v\n", x.IP, x.UpsilonDemon)
			}
		}

		//type.control
		if strings.HasPrefix(x.UpsilonDemon, "1") || strings.HasPrefix(x.ApcupsDemon, "1") {
			if (strings.HasPrefix(x.USB, "1")) {
				tmp = append(tmp, "SNMP/USB")
			} else {
				tmp = append(tmp, "SNMP")
			}
		} else {
			if strings.HasPrefix(x.USB, "1") && strings.HasPrefix(x.UpsilonDir, "1")  {
				tmp = append(tmp, "USB")
			}
			if strings.HasPrefix(x.UpsilonDir, "0") && strings.HasPrefix(x.ApcupsDir, "0") {
				tmp = append(tmp, "???")
			}
		}

		data = append(data, tmp)
	}
	for _, x := range data {
		table.Append(x)
	}
	table.Render()
}

//формирование таблицы по фильтрам где есть возможно проблемы
func (w *WorkApcups) GeneratingTableResulFAIL(out io.Writer) {
	table := tablewriter.NewWriter(out)
	table.SetHeader([]string{"Сервер (u/w)", "APCUPS (dir)", "APCUPS (demon)", "UPSILON (dir)", "UPSILON (demon)", "UPSILON (USB)", "CONTROL"})
	data := [][]string{}

	for _, x := range w.Result {
		tmp := []string{x.IP}

		//if 	(strings.HasPrefix(x.UpsilonDemon, "1") || strings.HasPrefix(x.ApcupsDemon,"1")  {
		//	tmp = append(tmp, "SNMP")
		//} else {
		//	if (strings.HasPrefix(x.USB, "1") && strings.HasPrefix(x.UpsilonDir, "1"))  {
		//		tmp = append(tmp, "USB")
		//	}
		//	if (strings.HasPrefix(x.UpsilonDir, "0") && strings.HasPrefix(x.ApcupsDir, "0"))  {
		//		tmp = append(tmp, "???")
		//	}
		//}
		if (strings.HasPrefix(x.ApcupsDemon, "0") && strings.HasPrefix(x.UpsilonDemon, "0") && strings.HasPrefix(x.USB, "0")) ||
			(strings.HasPrefix(x.ApcupsDir, "0") && strings.HasPrefix(x.UpsilonDir, "0")) {

			//if (strings.HasPrefix(x.ApcupsDir, "0") && strings.HasPrefix(x.UpsilonDir, "0")) ||
			//	(strings.HasPrefix(x.ApcupsDir, "1") && strings.HasPrefix(x.ApcupsDemon, "0") && strings.HasPrefix(x.UpsilonDir, "0")) ||
			//	(strings.HasPrefix(x.UpsilonDir, "1") && strings.HasPrefix(x.UpsilonDemon, "0") && strings.HasPrefix(x.ApcupsDir, "0")) ||
			//	(strings.HasPrefix(x.ApcupsDemon, "0") && strings.HasPrefix(x.UpsilonDemon, "0")) {

			//if strings.HasPrefix(x.ApcupsDir, "1") &&  strings.HasPrefix(x.ApcupsDemon, "0") ||
			//	strings.HasPrefix(x.ApcupsDir, "0") || strings.HasPrefix(x.UpsilonDir, "0") ||
			//	strings.HasPrefix(x.UpsilonDir, "1") &&  strings.HasPrefix(x.UpsilonDemon, "0") ||
			//	!strings.HasPrefix(x.ApcupsDir, "0") && !strings.HasPrefix(x.ApcupsDir, "1") ||
			//	!strings.HasPrefix(x.UpsilonDir, "0") && !strings.HasPrefix(x.UpsilonDir, "1") {

			//if strings.HasPrefix(x.ApcupsDir, "0")  ||  strings.HasPrefix(x.UpsilonDir, "0")   ||
			//	strings.HasPrefix(x.ApcupsDir, "0")  &&  strings.HasPrefix(x.UpsilonDir, "0") ||
			//	!strings.HasPrefix(x.ApcupsDir, "0") && !strings.HasPrefix(x.ApcupsDir, "1") ||
			//	!strings.HasPrefix(x.UpsilonDir, "0") && !strings.HasPrefix(x.UpsilonDir, "1") {

			//a.d
			if strings.HasPrefix(x.ApcupsDir, "0") {
				tmp = append(tmp, "-")
			} else if strings.HasPrefix(x.ApcupsDir, "1") {
				tmp = append(tmp, "+")
			} else {
				tmp = append(tmp, "err.check")
			}

			//a.s
			if strings.HasPrefix(x.ApcupsDemon, "0") {
				tmp = append(tmp, "-")
			} else if strings.HasPrefix(x.ApcupsDemon, "1") {
				tmp = append(tmp, "+")
			} else {
				tmp = append(tmp, "err.check")
			}

			//u.d
			if strings.HasPrefix(x.UpsilonDir, "0") {
				tmp = append(tmp, "-")
			} else if strings.HasPrefix(x.UpsilonDir, "1") {
				tmp = append(tmp, "+")
			} else {
				tmp = append(tmp, "err.check")
			}

			//u.s
			if strings.HasPrefix(x.UpsilonDemon, "0") {
				tmp = append(tmp, "-")
			} else if strings.HasPrefix(x.UpsilonDemon, "1") {
				tmp = append(tmp, "+")
			} else {
				tmp = append(tmp, "err.check")
			}

			//usb
			if strings.HasPrefix(x.USB, "0") {
				tmp = append(tmp, "-")
			} else if strings.HasPrefix(x.USB, "1") {
				tmp = append(tmp, "+")
			} else {
				tmp = append(tmp, "err.check[no usbutils pack]")
				if debuger {
					w.log.Printf("[debug] [%v] %v\n", x.IP, x.UpsilonDemon)
				}
			}

			//type.control
			if strings.HasPrefix(x.UpsilonDemon, "1") || strings.HasPrefix(x.ApcupsDemon, "1") {
				if (strings.HasPrefix(x.USB, "1")) {
					tmp = append(tmp, "SNMP/USB")
				} else {
					tmp = append(tmp, "SNMP")
				}
			} else {
				if strings.HasPrefix(x.USB, "1") && strings.HasPrefix(x.UpsilonDir, "1")  {
					tmp = append(tmp, "USB")
				}
				if strings.HasPrefix(x.UpsilonDir, "0") && strings.HasPrefix(x.ApcupsDir, "0") {
					tmp = append(tmp, "???")
				}
			}

			data = append(data, tmp)
		}
	}

	for _, x := range data {
		table.Append(x)
	}
	table.Render()
}

//формирование выходной таблицы со списком серверов куда не получилось подключиться из-за ошибок
func (w *WorkApcups) GeneratingTableResultERRORSERVERS(out io.Writer) {
	table := tablewriter.NewWriter(out)
	table.SetHeader([]string{"Сервер (u/w)", "PORT", "USER", "PASSWORD", "ERROR"})
	data := [][]string{}

	for _, x := range w.ErrorServers {
		tmp := []string{x.S.IP}
		tmp = append(tmp, x.S.Port)
		tmp = append(tmp, x.S.User)
		if x.S.Password != "" {
			tmp = append(tmp, "+")
		}
		tmp = append(tmp, x.Error)

		data = append(data, tmp)
	}
	for _, x := range data {
		table.Append(x)
	}
	table.Render()
}

func (w *WorkApcups) ShowResult() {
	for _, x := range w.Result {
		fmt.Printf("--------------------------------------------\n")
		fmt.Printf("==> %v\n", x.IP)

		if strings.HasPrefix(x.ApcupsDir, "0") {
			if w.cfg.Debug {
				fmt.Printf("==> APCUPS DIR: %v : [%v]\n", "отсутствует", x.ApcupsDir)
			} else {
				fmt.Printf("==> APCUPS DIR: %v\n", "отсутствует")
			}

		} else if strings.HasPrefix(x.ApcupsDir, "1") {
			if w.cfg.Debug {
				fmt.Printf("==> APCUPS DIR: %v : [%v]\n", "есть", x.ApcupsDir)
			} else {
				fmt.Printf("==> APCUPS DIR: %v\n", "есть")
			}

		} else {
			fmt.Printf("==> APCUPS DIR : [?] %v\n", x.ApcupsDir)

		}

		if strings.HasPrefix(x.ApcupsDemon, "0") {
			if w.cfg.Debug {
				fmt.Printf("==> APCUPS DEMON: %v : [%v]\n", "не запущен", x.ApcupsDemon)
			} else {
				fmt.Printf("==> APCUPS DEMON: %v\n", "не запущен")
			}

		} else if strings.HasPrefix(x.ApcupsDemon, "1") {
			if w.cfg.Debug {
				fmt.Printf("==> APCUPS DEMON: %v : [%v]\n", "запущен", x.ApcupsDemon)
			} else {
				fmt.Printf("==> APCUPS DEMON: %v\n", "запущен")
			}

		} else {
			fmt.Printf("==> APCUPS DEMON : [?] %v\n", x.ApcupsDemon)
		}

		if strings.HasPrefix(x.UpsilonDir, "0") {
			if w.cfg.Debug {
				fmt.Printf("==> UPSILON DIR: %v : [%v]\n", "отсутствует", x.UpsilonDir)
			} else {
				fmt.Printf("==> UPSILON DIR: %v\n", "отсутствует")
			}

		} else if strings.HasPrefix(x.UpsilonDir, "1") {
			if w.cfg.Debug {
				fmt.Printf("==> UPSILON DIR: %v : [%v]\n", "есть", x.UpsilonDir)
			} else {
				fmt.Printf("==> UPSILON DIR: %v\n", "есть")
			}

		} else {
			fmt.Printf("==> UPSILON DIR : [?] %v\n", x.UpsilonDir)
		}

		if strings.HasPrefix(x.UpsilonDemon, "0") {
			if w.cfg.Debug {
				fmt.Printf("==> UPSILON DEMON: %v : [%v]\n", "не запущен", x.UpsilonDemon)
			} else {
				fmt.Printf("==> UPSILON DEMON: %v\n", "не запущен")
			}

		} else if strings.HasPrefix(x.UpsilonDemon, "1") {
			if w.cfg.Debug {
				fmt.Printf("==> UPSILON DEMON: %v : [%v]\n", "запущен", x.UpsilonDemon)
			} else {
				fmt.Printf("==> UPSILON DEMON: %v\n", "запущен")
			}

		} else {
			fmt.Printf("==> UPSILON DEMON : [?] %v\n", x.UpsilonDemon)
		}
	}
}
func (w *WorkApcups) GeneratorConfig(inFile string) {
	f, err := os.OpenFile(inFile, os.O_RDWR, os.ModePerm)
	if err != nil {
		w.log.Panic(err)
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	stock := ConfigStruct{}
	for sc.Scan() {
		pp := strings.Split(sc.Text(), ":")
		s := server{
			IP:       pp[0],
			Port:     pp[1],
			Commands: nil,
		}
		com := make(map[string]string)
		com["upsdir"] = "/usr/sbin/test -d /etc/upsilon && echo 1 || echo 0"
		com["apsdir"] = "/usr/sbin/test -d /etc/apcupsd && echo 1 || echo 0"
		com["upsdemon"] = "/usr/sbin/ss -ulnp | /bin/grep upsilon > /dev/null && echo 1 || echo 0 "
		com["apsdemon"] = "/usr/sbin/ss -t4l | /bin/grep apcupsd > /dev/null && echo 1 || echo 0 "
		com["usb"] = "/usr/bin/lsusb | grep Prolific > /ev/null && echo 1 || echo 0"
		s.Commands = com
		stock.Servers = append(stock.Servers, &s)
	}
	enc := yaml.NewEncoder(os.Stdout)
	err = enc.Encode(stock)
	if err != nil {
		w.log.Panic(err)
	}
}
